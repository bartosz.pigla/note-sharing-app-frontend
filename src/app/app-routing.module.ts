import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {AddNoteComponent} from "./page/add-note/add-note.component";
import {NoteAddedConfirmationComponent} from "./page/note-added-confirmation/note-added-confirmation.component";
import {ViewNoteComponent} from "./page/view-note/view-note.component";

@NgModule({
  imports: [RouterModule.forRoot([
    {path: '', redirectTo: '/add-note', pathMatch: 'full'},
    {path: 'add-note', component: AddNoteComponent},
    {path: 'note-added-confirmation/:id', component: NoteAddedConfirmationComponent},
    {path: 'view-note/:id', component: ViewNoteComponent},
  ])],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
