import {Component} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ENVIRONMENT} from "../../environment/environment";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-note-added-confirmation',
  templateUrl: './note-added-confirmation.component.html',
  styleUrls: ['./note-added-confirmation.component.scss']
})
export class NoteAddedConfirmationComponent {

  noteUrl: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private snackBar: MatSnackBar,
  ) {
    this.noteUrl = `${ENVIRONMENT.frontendUrl}/view-note/${this.activatedRoute.snapshot.params['id']}`;
  }

  onLinkCopied() {
    this.snackBar.open("Link copied to clipboard")
  }

}
