import {Component} from '@angular/core';
import {ViewNoteService} from "./view-note.service";
import {ActivatedRoute} from "@angular/router";
import {NoteResponseDto} from "../../dto/note-response.dto";

@Component({
  selector: 'app-view-note',
  templateUrl: './view-note.component.html',
  styleUrls: ['./view-note.component.scss']
})
export class ViewNoteComponent {

  password?: string;
  note?: NoteResponseDto;
  isLoading = false;
  private readonly id: string;

  constructor(
    private viewNoteService: ViewNoteService,
    private activatedRoute: ActivatedRoute,
  ) {
    this.id = this.activatedRoute.snapshot.params['id'];
  }

  getNote() {
    this.isLoading = true;
    this.viewNoteService.getNote(this.id, this.password!!)
      .subscribe({
        next: (note: NoteResponseDto) => this.note = note,
        complete: () => this.isLoading = false,
      });
  }

}
