import {Injectable} from "@angular/core";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {MatSnackBar} from "@angular/material/snack-bar";
import {catchError, EMPTY, Observable, take} from "rxjs";
import {NoteResponseDto} from "../../dto/note-response.dto";
import {ENVIRONMENT} from "../../environment/environment";

@Injectable({
  providedIn: 'root',
})
export class ViewNoteService {

  constructor(
    private httpClient: HttpClient,
    private snackBar: MatSnackBar,
  ) {
  }

  getNote(id: string, password: string): Observable<NoteResponseDto> {
    return this.httpClient.post<NoteResponseDto>(`${ENVIRONMENT.backendUrl}/note/${id}`, {password}).pipe(
      take(1),
      catchError((error: HttpErrorResponse) => {
        this.snackBar.open(`Failed to get note. Error message: ${error.statusText}`);
        return EMPTY;
      })
    );
  }

}
