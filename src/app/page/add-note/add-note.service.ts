import {Injectable} from "@angular/core";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {AddNoteForm} from "./add-note.form";
import {AddNoteRequestDto} from "./add-note-request.dto";
import {catchError, EMPTY, Observable, take, tap} from "rxjs";
import {NoteResponseDto} from "../../dto/note-response.dto";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Router} from "@angular/router";
import {ENVIRONMENT} from "../../environment/environment";

@Injectable({
  providedIn: 'root',
})
export class AddNoteService {

  constructor(
    private httpClient: HttpClient,
    private snackBar: MatSnackBar,
    private router: Router,
  ) {
  }

  addNote(form: AddNoteForm): Observable<NoteResponseDto> {
    const dto: AddNoteRequestDto = this.createDtoFromForm(form);

    return this.httpClient.post<NoteResponseDto>(`${ENVIRONMENT.backendUrl}/note`, dto).pipe(
      take(1),
      tap((data: NoteResponseDto) => {
        return this.router.navigate(['/note-added-confirmation', data.id]);
      }),
      catchError((error: HttpErrorResponse) => {
        this.snackBar.open(`Failed to add note. Error message: ${error.statusText}`);
        return EMPTY;
      })
    );
  }

  private createDtoFromForm(form: AddNoteForm): AddNoteRequestDto {
    return {
      title: form.controls.title.value!!,
      content: form.controls.content.value!!,
      expiryDate: form.controls.expiryDate.value!!,
      password: form.controls.password.controls.password.value!!
    }
  }

}
