export interface AddNoteRequestDto {
  title: string,
  content: string,
  password: string,
  expiryDate: Date,
}
