import {FormControl, FormGroup} from "@angular/forms";
import {PasswordForm} from "../../form/password.form";

export type AddNoteForm = FormGroup<{
  title: FormControl<string | null>,
  content: FormControl<string | null>,
  expiryDate: FormControl<Date | null>,
  password: PasswordForm
}>
