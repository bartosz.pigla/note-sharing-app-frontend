import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AddNoteForm} from "./add-note.form";
import {AddNoteService} from "./add-note.service";
import {addDays} from 'date-fns';
import {dateRangeValidator} from "../../validator/date-range.validator";
import {passwordConfirmationValidator} from "../../validator/password-confirmation.validator";

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddNoteComponent {

  private readonly EXPIRY_DATE_MAX_AGE = 10;
  readonly TITLE_MAX_LENGTH = 100;
  readonly CONTENT_MAX_LENGTH = 3_000;
  readonly MIN_EXPIRY_DATE = addDays(new Date(), 1);
  readonly MAX_EXPIRY_DATE = addDays(new Date(), this.EXPIRY_DATE_MAX_AGE);
  readonly PASSWORD_MIN_LENGTH = 10;
  readonly PASSWORD_MAX_LENGTH = 50;
  isLoading = false;
  addNoteForm: AddNoteForm = new FormGroup({
    title: new FormControl('', [Validators.required, Validators.maxLength(this.TITLE_MAX_LENGTH)]),
    content: new FormControl('', [Validators.required, Validators.maxLength(this.CONTENT_MAX_LENGTH)]),
    expiryDate: new FormControl(
      this.MIN_EXPIRY_DATE,
      [
        Validators.required,
        dateRangeValidator(this.MIN_EXPIRY_DATE, this.MAX_EXPIRY_DATE)
      ]
    ),
    password: new FormGroup({
      password: new FormControl(
        '',
        [
          Validators.required,
          Validators.minLength(this.PASSWORD_MIN_LENGTH),
          Validators.maxLength(this.PASSWORD_MAX_LENGTH)
        ]
      ),
      passwordConfirmation: new FormControl(
        '',
        [
          Validators.required,
          Validators.minLength(this.PASSWORD_MIN_LENGTH),
          Validators.maxLength(this.PASSWORD_MAX_LENGTH)
        ]
      ),
    }, [Validators.required, passwordConfirmationValidator()])
  });

  constructor(
    private service: AddNoteService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
  }

  submit() {
    this.isLoading = true;
    this.service.addNote(this.addNoteForm).subscribe({
      complete: () => {
        this.isLoading = false;
        this.changeDetectorRef.detectChanges();
      }
    });
  }

}
