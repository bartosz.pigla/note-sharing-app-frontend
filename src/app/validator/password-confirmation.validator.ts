import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";
import {PasswordForm} from "../form/password.form";

export function passwordConfirmationValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const formGroup = control as PasswordForm;

    if (formGroup.controls.password.value !== formGroup.controls.passwordConfirmation.value) {
      return {confirmationMismatch: {value: control.value}}
    }

    return null;
  }
}
