import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";
import {isAfter, isBefore} from 'date-fns';

export function dateRangeValidator(minDate: Date, maxDate: Date): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if (isBefore(control.value, minDate)) {
      return {dateRange: {value: control.value}}
    }

    if (isAfter(control.value, maxDate)) {
      return {dateRange: {value: control.value}}
    }

    return null;
  }
}
