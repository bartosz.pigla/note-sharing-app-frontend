import {FormControl, FormGroup} from "@angular/forms";

export type PasswordForm = FormGroup<{
  password: FormControl<string | null>,
  passwordConfirmation: FormControl<string | null>,
}>
