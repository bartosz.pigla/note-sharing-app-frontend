export interface NoteResponseDto {
  id: string,
  title: string,
  content: string,
}
